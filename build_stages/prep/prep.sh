#!/bin/bash -vx

cd /tmp/build && \
apt-get install -y wget && \
wget -c --tries=5 http://apt.puppetlabs.com/puppetlabs-release-pc1-precise.deb &&
dpkg -i puppetlabs-release-pc1*.deb && \
apt-get update && \
apt-get -y upgrade && \
apt-get install puppet-agent && \
ln -s /opt/puppetlabs/puppet/bin/puppet /usr/local/bin/puppet && \
puppet module install puppetlabs-stdlib && \
puppet module install nanliu-staging
