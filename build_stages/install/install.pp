$build_dir = '/tmp/build'

# This is where the sqoop versions will live.
$sqoop_install_dir = '/opt/sqoop'
file { $sqoop_install_dir: ensure => directory, }


# Cache things in the temporary build directory for
# easy expunge.j
class { '::staging': path => "${build_dir}/staging", }


# Create a cache of the specified Hadoop versions.
$hadoop_versions = {
  'sqoop-1.4.6.tar.gz' => {
    'source' => 'http://www.trieuvan.com/apache/sqoop/1.4.6/sqoop-1.4.6.tar.gz', },
}
$hadoop_versions.each |$sqoop_version, $sqoop_attributes| {
  staging::deploy { $sqoop_version:
    target => "${sqoop_install_dir}",
    *      => $sqoop_attributes,
  }
}
