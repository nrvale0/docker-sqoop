#!/bin/bash -xv

apt-get remove --purge -y puppet-agent puppetlabs-release-pc1 && \
apt-get autoremove -y && \
apt-get clean all -y && \
rm -rf /usr/local/bin/puppet && \
rm -rf /tmp/build
