FROM ubuntu:14.04

MAINTAINER Nathan Valentine <nrvale0@gmail.com>

ENV PATH /usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin
ENV TERM ansi
ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_NONINTERACTIVE_SEEN true

ADD ./build_stages/prep /tmp/build/prep
RUN /tmp/build/prep/prep.sh

ADD ./build_stages/install /tmp/build/install
RUN /tmp/build/install/install.sh

ADD ./build_stages/clean /tmp/build/clean
RUN /tmp/build/clean/clean.sh
